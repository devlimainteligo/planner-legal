import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _6fae6e74 = () => interopDefault(import('..\\pages\\Archivados\\index.vue' /* webpackChunkName: "pages_Archivados_index" */))
const _e0aac93a = () => interopDefault(import('..\\pages\\Pendientes\\index.vue' /* webpackChunkName: "pages_Pendientes_index" */))
const _30a4c35c = () => interopDefault(import('..\\pages\\Reportes\\index.vue' /* webpackChunkName: "pages_Reportes_index" */))
const _5332e3c4 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/Archivados",
    component: _6fae6e74,
    name: "Archivados"
  }, {
    path: "/Pendientes",
    component: _e0aac93a,
    name: "Pendientes"
  }, {
    path: "/Reportes",
    component: _30a4c35c,
    name: "Reportes"
  }, {
    path: "/",
    component: _5332e3c4,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
