import Vue from "vue";
import { GridPlugin, Toolbar, ExcelExport } from '@syncfusion/ej2-vue-grids';

Vue.use(GridPlugin);
Vue.use(Toolbar);
Vue.use(ExcelExport);