import firebase from 'firebase'
require('firebase/auth')
require('firebase/firestore')
require('firebase/storage')
require('firebase/performance')

const firebaseConfig = {
  apiKey: "AIzaSyCMgOjjK0TwoGo9rxIjh6M-bkmAokQHtIQ",
  authDomain: "planner-legal.firebaseapp.com",
  databaseURL: "https://planner-legal.firebaseio.com",
  projectId: "planner-legal",
  storageBucket: "planner-legal.appspot.com",
  messagingSenderId: "593594999542",
  appId: "1:593594999542:web:dd3260610a63ac779dc2d7",
  measurementId: "G-W6LBGW1LFP"
  // apiKey: "AIzaSyB5eNvm1Vm-VdMCw0_AkiVbweu4bdwvCAc",
  // authDomain: "planer-apoyo.firebaseapp.com",
  // databaseURL: "https://planer-apoyo.firebaseio.com",
  // projectId: "planer-apoyo",
  // storageBucket: "planer-apoyo.appspot.com",
  // messagingSenderId: "315222210608",
  // appId: "1:315222210608:web:cc7d23ca3dc3685e2795f5"
}

let app = null
if (!firebase.apps.length) {
  console.log(!firebase.apps.length);
  app = firebase.initializeApp(firebaseConfig)
}

const fs = app.firestore()
const auth = app.auth()
const storage = app.storage()
const perf = app.performance()
const db = app.database()
export {db, storage, perf, auth, fs}