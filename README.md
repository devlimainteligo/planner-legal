### Características del proyecto:

1. Framework SSR:

    NuxtJS 2.14.5
    Framework basado en Vuejs (https://nuxtjs.org/docs/2.x/get-started/installation)

2. Base de datos

    Firebase - firestore

4. UI framework

    Vuetify (https://vuetifyjs.com/en/)

5. Características

    Axios: Http request 


### `firebase deploy`
Levantar proyecto en local (running on http://localhost:3000.)
Ejecutar el siguiente comando:
npm run dev

### `firebase deploy`

El proyecto está hosteado en firebase hosting por lo cual primero hay que logearse en  firebase<br>
firebase login -> correo inteligo.digital@gmail.com 
firebase init -> inicializar el proyecto
firebase deploy --only hosting
