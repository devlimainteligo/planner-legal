import { fs } from "@/plugins/firebase.js";
import firebase from "firebase";

export const state = () => ({
    pendientes: [],
    areas: '',
    empresa: '',
    responsables: '',
    prioridad: '',
    status: '',
    urgencia: '',
    dedicacion: '',
    canchas: '',
    otros: '',
    tema: '',
    editarPendiente: [],
    pendiente: '',
    modall: false,
    archivos: '',
    mostrarfiltros: 0,
    arrPendientesFiltrados: [],
    arrArchivosFiltrados: [],
    arrPendFiltradosRepor: [],
    arrArchFiltradosRepor: [],
    sinLeer: [],
    token: '',
    tipoFiltrado:'',
    dataModal: [],
    colorPrioridad:''
})
export const mutations = {
    /*  :::::::::::::::::::::::::::::::::::::::
            mutaciones para select
       ::::::::::::::::::::::::::::::::::::::::::::*/
    setArea(state, payload) {
        state.areas = payload
    },
    setTipoFiltrado(state, payload) {
        state.tipoFiltrado = payload
    },
    setEmpresa(state, payload) {
        state.empresa = payload
    },
    setPrioridad(state, payload) {
        state.prioridad = payload
    },
    setUrgencia(state, payload) {
        state.urgencia = payload
    },
    setStatus(state, payload) {
        state.status = payload
    },
    setTema(state, payload) {
        state.tema = payload
    },
    setResponsable(state, payload) {
        state.responsables = payload
    },
    setCancha(state, payload) {
        state.canchas = payload
    },
    setOtros(state, payload) {
        state.otros = payload
    },
    setDedicacion(state, payload) {
        state.dedicacion = payload
    },
    /*  :::::::::::::::::::::::::::::::::::::::::::::
             mutaciones pa...
        ::::::::::::::::::::::::::::::::::::::::::::*/
    setSinLeer(state, payload) {
        state.sinLeer = payload
    },
    setToken(state, payload) {
        state.token = payload
    },
    setmostrarfiltros(state, payload) {
        state.mostrarfiltros = payload
    },
    setPendientes(state, payload) {
        state.pendientes = payload
    },
    setPendiente(state, payload) {
        state.pendiente = payload
    },

    setModal(state, payload) {
        state.modall = payload
    },
    setArchivados(state, payload) {
        state.archivos = payload
    },
    setArrPendientesFiltrados(state, payload) {
        state.arrPendientesFiltrados = payload;
    },
    setArrArchivosFiltrados(state, payload) {
        console.log(payload, 'que hay por aqui');
        state.arrArchivosFiltrados = payload;
    },
    archivarPendiente(state, payload) {
        state.pendientes = state.pendientes.splice(payload, 1);
    },
    setArrPendienteFiltradosRepor(state, payload) {
        state.arrPendFiltradosRepor = payload;
    },
    setArrArchivosFiltradosRepor(state, payload) {
        state.arrArchFiltradosRepor = payload;
    },
    setCambiosModal(state, payload) {
        state.dataModal = payload;
    },
    setColorPrioridad(state, payload){
      state.colorPrioridad = payload
    }
}

export const actions = {
    /*  :::::::::::::::::::::::::::::::::::::::
           1. DATA AREA 
    ::::::::::::::::::::::::::::::::::::::::::::*/
    getArea(context) {
        //console.log('entro al getarea');

        fs.collection('area').onSnapshot(docs => {
                const areas = []
                docs.forEach(e => {
                    areas.push(e.data())
                });
                return context.commit('setArea', areas);
            })
    },
    /*  :::::::::::::::::::::::::::::::::::::::
          2.  DATA EMPRESA
    ::::::::::::::::::::::::::::::::::::::::::::*/
    getEmpresa(context) {
        fs.collection('empresa').onSnapshot(docs => {
                const empresas = []
                docs.forEach(e => {

                    empresas.push(e.data())
                });

                return context.commit('setEmpresa', empresas);
            })
    },
    /*  :::::::::::::::::::::::::::::::::::::::
          3.  DATA RESPONSABLES
    ::::::::::::::::::::::::::::::::::::::::::::*/
    getResponsable(context) {

        fs.collection('responsable').onSnapshot(docs => {
                const responsables = []
                docs.forEach(e => {

                    responsables.push(e.data())
                });

                return context.commit('setResponsable', responsables);
            })
    },
    /*  :::::::::::::::::::::::::::::::::::::::
             3.  DATA PRIORIDAD
       ::::::::::::::::::::::::::::::::::::::::::::*/
    getPrioridad(context) {

        fs.collection('prioridad').onSnapshot(docs => {
                const prioridades = []
                docs.forEach(e => {

                    prioridades.push(e.data())
                });

                return context.commit('setPrioridad', prioridades);
            })
    },
    /*  :::::::::::::::::::::::::::::::::::::::
          4.  DATA URGENCIA
    ::::::::::::::::::::::::::::::::::::::::::::*/

    getUrgencia(context) {

        fs.collection('urgencia').onSnapshot(docs => {
            const urgencias = []
            docs.forEach(e => {
                urgencias.push(e.data())
            });

                return context.commit('setUrgencia', urgencias);
            })
    },

    /*  :::::::::::::::::::::::::::::::::::::::
          5.  DATA STATUS
    ::::::::::::::::::::::::::::::::::::::::::::*/

    getStatus(context) {

        fs.collection('status').onSnapshot(docs => {
                const statuss = []
                docs.forEach(e => {

                    statuss.push(e.data())
                });

                return context.commit('setStatus', statuss);
            })
    },

    /*  :::::::::::::::::::::::::::::::::::::::
          6.   DATA TEMA EN CARGO DE 
     ::::::::::::::::::::::::::::::::::::::::::::*/

    getTemaEncargo(context) {

        fs.collection('TemaEncargo').onSnapshot(docs => {
                const temas = []
                docs.forEach(e => {

                    temas.push(e.data())
                });

                return context.commit('setTema', temas);
            })
    },
    /*  :::::::::::::::::::::::::::::::::::::::
         7.   DATA EN CANCHA DE  🙂 
    ::::::::::::::::::::::::::::::::::::::::::::*/

    getCancha(context) {

        fs.collection('EnCanchaDe').onSnapshot(docs => {
                const canchass = []
                docs.forEach(e => {

                    canchass.push(e.data())
                });

                return context.commit('setCancha', canchass);
            })
    },
    /*  :::::::::::::::::::::::::::::::::::::::
             8.   DATA OTROS
        ::::::::::::::::::::::::::::::::::::::::::::*/

    getOtros(context) {

        fs.collection('otros').onSnapshot(docs => {
                const otross = []
                docs.forEach(e => {

                    otross.push(e.data())
                });

                return context.commit('setOtros', otross);
            })
    },
    /*  :::::::::::::::::::::::::::::::::::::::
            9.   DATA DEDICACION
       ::::::::::::::::::::::::::::::::::::::::::::*/

    getDedidacion(context) {

        fs.collection('dedicacion').onSnapshot(docs => {
                const dedicaciones = []
                docs.forEach(e => {

                    dedicaciones.push(e.data())
                });

                return context.commit('setDedicacion', dedicaciones);
            })
    },
    /*  :::::::::::::::::::::::::::::::::::::::
            10.   DATA COLOR PRIORIDAD
       ::::::::::::::::::::::::::::::::::::::::::::*/

    getColorPrioridad(context) {

      fs.collection('colorprioridad').onSnapshot(docs => {
        const colorprioridad = []
        docs.forEach(e => {

          colorprioridad.push(e.data())
        });

        return context.commit('setColorPrioridad', colorprioridad);
      })
    },



    editarPendientes(context, pendiente) {
        context.commit('getPendiente', pendiente)
    },
    getPendientes(context) {

        fs.collection("pendientes").onSnapshot(docs => {
            const sinLeer = [];
            const tareas = [];
            docs.forEach(e => {
                const elemento = { id: e.id, ...e.data() };
                tareas.push(elemento);
                if (e.data().leido == false) sinLeer.push(elemento)
            });
            context.commit("setSinLeer", sinLeer);
            return context.commit("setPendientes", tareas);
        });
    },
    getPendientesFiltrados(context) {
        fs.collection("pendientes").onSnapshot(docs => {
            const tareas = [];
            docs.forEach(e => {
                const elemento = { id: e.id, ...e.data() };
                tareas.push(elemento);
            });
            return context.commit("setPendientes", tareas);
        });
    },

    getArchivos(context) {
        fs.collection("Archivados").onSnapshot(docs => {
            const tareas = [];
            docs.forEach(e => {
                const elemento = { id: e.id, ...e.data() };
                tareas.push(elemento);
            });
            return context.commit("setArchivados", tareas);
        });
    },
    getArchivosFiltrados(context) {
        fs.collection("Archivados").onSnapshot(docs => {
            const tareas = [];
            docs.forEach(e => {
                const elemento = { id: e.id, ...e.data() };
                tareas.push(elemento);
            });
            return context.commit("setArchivados", tareas);
        });
    }

}